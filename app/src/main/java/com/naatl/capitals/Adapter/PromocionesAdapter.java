package com.naatl.capitals.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.naatl.capitals.Activity.DetailsActivity;
import com.naatl.capitals.Models.Promocion;
import com.naatl.capitals.R;

import java.util.ArrayList;

/**
 * Created by SACM on 28/10/2017.
 */

public class PromocionesAdapter extends RecyclerView.Adapter<PromocionesAdapter.PromocionesViewHolder> {
    private ArrayList<Promocion> promocionArrayList;
    private int resource;
    private Activity activity;


    public PromocionesAdapter(ArrayList<Promocion> promocionArrayList, int resource, Activity activity) {
        this.promocionArrayList = promocionArrayList;
        this.resource = resource;
        this.activity = activity;
    }

    @Override
    public PromocionesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(resource,parent,false);
        return new PromocionesAdapter.PromocionesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PromocionesViewHolder holder, int position) {
        final Promocion promocion = promocionArrayList.get(position);
        holder.imgpromo1.setImageResource(promocion.getImagen1());
        holder.textViewPromoNombre1.setText(""+promocion.getNombrePromocion1());
        holder.textViewPromoDesc1.setText(""+promocion.getTipoPromocion1());

        if (promocion.getId2()>=1){
            holder.imgpromo2.setImageResource(promocion.getImagen2());
            holder.textViewPromoNombre2.setText(""+promocion.getNombrePromocion2());
            holder.textViewPromoDesc2.setText(""+promocion.getTipoPromocion2());
        }else {
            holder.imgpromo2.setImageResource(promocion.getImagen1());
            holder.textViewPromoNombre2.setText(""+promocion.getNombrePromocion1());
            holder.textViewPromoDesc2.setText(""+promocion.getTipoPromocion1());
        }


        holder.cardpromo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(activity, DetailsActivity.class);
                intent.putExtra("Id",String.valueOf(promocion.getId1()));
               activity.startActivity(intent);
            }
        });

        holder.cardpromo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             Intent intent = new Intent(activity, DetailsActivity.class);
                intent.putExtra("Id",String.valueOf(promocion.getId2()));
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return  promocionArrayList.size();
    }

    public class PromocionesViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgpromo1;
        private TextView textViewPromoNombre1;
        private TextView textViewPromoDesc1;
        private ImageView imgpromo2;
        private TextView textViewPromoNombre2;
        private TextView textViewPromoDesc2;
        private CardView cardpromo1;
        private CardView cardpromo2;
        public PromocionesViewHolder(View itemView) {
            super(itemView);
            imgpromo1= (ImageView) itemView.findViewById(R.id.imgpromo1);
            textViewPromoNombre1= (TextView) itemView.findViewById(R.id.textViewPromoNombre1);
            textViewPromoDesc1= (TextView) itemView.findViewById(R.id.textViewPromoDesc1);
            imgpromo2= (ImageView) itemView.findViewById(R.id.imgpromo2);
            textViewPromoNombre2= (TextView) itemView.findViewById(R.id.textViewPromoNombre2);
            textViewPromoDesc2= (TextView) itemView.findViewById(R.id.textViewPromoDesc2);
            cardpromo1= (CardView) itemView.findViewById(R.id.cardpromo1);
            cardpromo2= (CardView) itemView.findViewById(R.id.cardpromo2);
        }
    }
}
