package com.naatl.capitals.Services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SACM on 28/10/2017.
 */

public class AgenteMovil {

    public static final String BASE_URL="https://agentemovil.pagatodo.com/AgenteMovil.svc/agMov/";



    private static Retrofit retrofit=null;

    public static Retrofit getApi(){
        if(retrofit==null){

            retrofit =new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return  retrofit;
    }
}
