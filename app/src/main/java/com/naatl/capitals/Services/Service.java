package com.naatl.capitals.Services;

import com.naatl.capitals.Models.LoginM;
import com.naatl.capitals.Models.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by SACM on 28/10/2017.
 */

public interface Service {
    @Headers({
            "SO: Android",
            "Version: 2.5.2"
    })
    @POST("login")
    Call<List<LoginResponse>> validaCorreo(@Body LoginM data);
}
