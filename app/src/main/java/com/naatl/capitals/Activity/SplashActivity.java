package com.naatl.capitals.Activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;

import com.naatl.capitals.R;

import java.security.MessageDigest;

public class SplashActivity extends AppCompatActivity {
    private final int DURACION_SPLASH_LOGIN =5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable(){
            public void run(){
                Intent intent=new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            };
        }, DURACION_SPLASH_LOGIN);
    }

    public String key(){
        PackageInfo info;
        String KeyHashes = null;
        try{
            info = getPackageManager().getPackageInfo("com.naatl.splash", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures){
                MessageDigest md;
                md=MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                KeyHashes = new String(Base64.encode(md.digest(),0));
            }
        }catch (Exception e){
            System.out.println(""+e);
        }
        return KeyHashes;
    }
}
