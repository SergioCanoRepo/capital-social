package com.naatl.capitals.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.naatl.capitals.Models.LoginM;
import com.naatl.capitals.Models.LoginResponse;
import com.naatl.capitals.R;
import com.naatl.capitals.Services.AgenteMovil;
import com.naatl.capitals.Services.Service;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private CallbackManager callbackManager;

    private LoginButton buttonFacebook;
    private TextView textUsuario;
    private TextView textMostrar;
    private TextInputEditText txtUser;
    private TextInputEditText txtPass;
    private Typeface face;
    private int muestrapassword=0;
    private Button buttonIngresarLogin;
    private Button buttonRegistrarLogin;
    private TextView textOlvidePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        callbackManager = CallbackManager.Factory.create();
        face=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Medium.ttf");

        buttonFacebook = (LoginButton) findViewById(R.id.buttonFacebook);
        textUsuario = (TextView) findViewById(R.id.textUsuario);
        txtUser = (TextInputEditText) findViewById(R.id.txtUser);
        txtPass = (TextInputEditText) findViewById(R.id.txtPass);
        textMostrar = (TextView) findViewById(R.id.textMostrar);
        buttonRegistrarLogin = (Button) findViewById(R.id.buttonRegistrarLogin);
        buttonIngresarLogin = (Button) findViewById(R.id.buttonIngresarLogin);
        textOlvidePass= (TextView) findViewById(R.id.textOlvidePass);

        buttonFacebook.setTypeface(face);
        textUsuario.setTypeface(face);
        txtUser.setTypeface(face);
        txtPass.setTypeface(face);
        textMostrar.setTypeface(face);
        buttonRegistrarLogin.setTypeface(face);
        buttonIngresarLogin.setTypeface(face);
        textOlvidePass.setTypeface(face);

        buttonFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Intent intent =new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCancel() {
                muestraMensaje("Debe aceptar para poder ingresar");
            }

            @Override
            public void onError(FacebookException error) {
                muestraMensaje("Error en la comunicación con Facebook");
                System.out.println("ERROR "+error);
            }
        });


        textMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (muestrapassword==0){
                    txtPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    textMostrar.setText("OCULTAR");
                    muestrapassword=1;
                }else{
                    txtPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    textMostrar.setText("MOSTRAR");
                    muestrapassword=0;
                }
            }
        });


        buttonIngresarLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = txtUser.getText().toString();
                String pass = txtPass.getText().toString();

                if (usuario.equals("")||pass.equals("")){
                    muestraMensaje("Introduzca Usuario/Contraseña");
                }else{
                    validaUsuario(pass,usuario);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    private  void muestraMensaje(String mensaje){
        Toast.makeText(LoginActivity.this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private void validaUsuario(String pass, String correo){
        LoginM data = new LoginM(pass,correo);
        Service service = AgenteMovil.getApi().create(Service.class);
        Call<List<LoginResponse>> serviciosCall = service.validaCorreo(data);
        serviciosCall.enqueue(new Callback<List<LoginResponse>>() {
            ProgressDialog progress=ProgressDialog.show(
                    LoginActivity.this
                    ,"Espere"
                    ,"Conectando..."
            );
            @Override
            public void onResponse(Call<List<LoginResponse>> call, Response<List<LoginResponse>> response) {
                progress.dismiss();
                //Tuve problemas para conectar el servicio por eso lo pasa directo los headers y parametros son los que piden en el documento
                Intent intent =new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
                if (response.isSuccessful()) {
                    List<LoginResponse> services = response.body();
                    for (LoginResponse loginResponse : services) {

                    }
                }else{
                    System.out.println("Error");
                    System.out.println(response.code());
                    System.out.println(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<LoginResponse>> call, Throwable t) {
                progress.dismiss();
                muestraMensaje("Error de comunicación con el servicio");
            }
        });

    }
}
