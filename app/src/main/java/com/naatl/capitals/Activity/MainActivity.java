package com.naatl.capitals.Activity;

import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;

import com.facebook.login.LoginManager;
import com.naatl.capitals.Adapter.PromocionesAdapter;
import com.naatl.capitals.DbManager.DbManager;
import com.naatl.capitals.Models.Promo;
import com.naatl.capitals.Models.Promocion;
import com.naatl.capitals.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    private Typeface face;
    private EditText txtBuscarMain;
    private RecyclerView recyclerPromo;
    private ImageView imgBuscarMain;
    private ArrayList<Promocion> promocionArrayList = new ArrayList<>();
    private PromocionesAdapter promocionesAdapter;
    private DbManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Solo lo pongo para que cierre sesion y no se quede activa
        LoginManager.getInstance().logOut();
        manager = new DbManager(this);
        manager.generaPromocione();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        showToolbar( toolbar,false);
        face= Typeface.createFromAsset(getAssets(),"fonts/Roboto-Light.ttf");


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        txtBuscarMain = (EditText) findViewById(R.id.txtBuscarMain);
        recyclerPromo = (RecyclerView) findViewById(R.id.recyclerPromo);
        imgBuscarMain = (ImageView) findViewById(R.id.imgBuscarMain);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerPromo.setLayoutManager(layoutManager);
        promocionesAdapter =
                new PromocionesAdapter(promocionArrayList, R.layout.cardview_promociones, MainActivity.this);
        recyclerPromo.setAdapter(promocionesAdapter);
        generaPromociones(0,"");
        txtBuscarMain.setTypeface(face);

        imgBuscarMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtBuscarMain.getText().equals("")){

                    generaPromociones(0,"");


                }else{

                    generaPromociones(1,txtBuscarMain.getText().toString());

                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void showToolbar( Toolbar toolbar,boolean upButton) {

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void generaPromociones(int valida,String busca){
        promocionArrayList.clear();
        ArrayList<Promo> promos = new ArrayList<>();
        Cursor cursor=null;
        if(valida==0){
            cursor = manager.get_Promociones();
        }else{
            cursor = manager.get_Promociones(busca);
        }
        if (cursor!=null || cursor.getCount()>=1){
            System.out.println("----"+cursor.getCount());
            while (cursor.moveToNext()){
                Promo promo = new Promo();
                promo.setId(cursor.getInt(0));
                promo.setNombrePromocion(cursor.getString(1));
                promo.setTipoPromocion(cursor.getString(2));
                promo.setImagen(cursor.getInt(3));
                promo.setLogo(cursor.getInt(4));
                promos.add(promo);

            }
            int count = promos.size();
            if ((count % 2)==0){
                for (int i=0;i<count;i= i+2){
                    Promo promo = new Promo();
                    Promo promo1 = new Promo();
                    promo = promos.get(i);
                    promo1 = promos.get(i+1);
                    Promocion promocion =
                            new Promocion(promo.getId(),promo.getNombrePromocion(),promo.getTipoPromocion(),promo.getImagen(),promo.getLogo(),
                                    promo1.getId(),promo1.getNombrePromocion(),promo1.getTipoPromocion(),promo1.getImagen(),promo1.getLogo() );
                    promocionArrayList.add(promocion);
                }
                promocionesAdapter.notifyDataSetChanged();

            }else{
                for (int i=0;i<count;i= i+2){
                    Promo promo = new Promo();
                    Promo promo1 = new Promo();
                    promo = promos.get(i);
                    if (i<(count-1)){
                        promo1 = promos.get(i+1);
                    }else {
                        promo1 =promo;
                    }
                    Promocion promocion =
                            new Promocion(promo.getId(),promo.getNombrePromocion(),promo.getTipoPromocion(),promo.getImagen(),promo.getLogo(),
                                    promo1.getId(),promo1.getNombrePromocion(),promo1.getTipoPromocion(),promo1.getImagen(),promo1.getLogo() );
                    promocionArrayList.add(promocion);
                }
                promocionesAdapter.notifyDataSetChanged();
            }

        }else {
            generaPromociones(0,"");
        }



    }
}
