package com.naatl.capitals.Activity;

import android.animation.Animator;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.naatl.capitals.DbManager.DbManager;
import com.naatl.capitals.Models.Promo;
import com.naatl.capitals.Models.Promocion;
import com.naatl.capitals.R;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class DetailsActivity extends AppCompatActivity {

    private FabSpeedDial fabSpeedDial;
    private boolean isOpen = false;
    private RelativeLayout layoutButtons;
    private CoordinatorLayout layoutMain;
    private ImageView imgeLogo;
    private TextView promoDetail;
    private TextView promoDetailPr;
    private TextView promoDetailLo;
    private Typeface medium;
    private Typeface light;
    private String id;
    private DbManager manager;
    private Promo promocion;
    private ImageView imageheader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        showToolbar(toolbar, true);
        manager = new DbManager(this);
        medium= Typeface.createFromAsset(getAssets(),"fonts/Roboto-Medium.ttf");
        light= Typeface.createFromAsset(getAssets(),"fonts/Roboto-Light.ttf");
        Intent intent = getIntent();
        if(intent != null){
            id=intent.getStringExtra("Id");
            Cursor cursor = manager.get_Promo(id);
            while (cursor.moveToNext()){
                promocion =new Promo();
                promocion.setId(cursor.getInt(0));
                promocion.setNombrePromocion(cursor.getString(1));
                promocion.setTipoPromocion(cursor.getString(2));
                promocion.setImagen(cursor.getInt(3));
                promocion.setLogo(cursor.getInt(4));
            }

        }


        layoutButtons = (RelativeLayout) findViewById(R.id.layoutButtons);
        layoutMain = (CoordinatorLayout) findViewById(R.id.layoutMain);
        imgeLogo = (ImageView) findViewById(R.id.imgeLogo);
        imageheader = (ImageView) findViewById(R.id.imageheader);
        promoDetail = (TextView) findViewById(R.id.promoDetail);
        promoDetailPr = (TextView) findViewById(R.id.promoDetailPr);
        promoDetailLo = (TextView) findViewById(R.id.promoDetailLo);

        promoDetail.setTypeface(medium);
        promoDetailPr.setTypeface(light);
        promoDetailLo.setTypeface(light);

        promoDetail.setText(promocion.getNombrePromocion());
        promoDetailPr.setText(promocion.getTipoPromocion());
        imgeLogo.setImageResource(promocion.getLogo());
        imageheader.setImageResource(promocion.getImagen());



        fabSpeedDial = (FabSpeedDial) findViewById(R.id.fab_speed_dial);
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                viewMenu();
                return true;
            }

            @Override
            public void onMenuClosed() {
                super.onMenuClosed();

                viewMenu();
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                viewMenu();
                return false;
            }
        });




    }


    public void showToolbar( Toolbar toolbar,boolean upButton) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }



    private void viewMenu() {
        if(Build.VERSION.SDK_INT>=21) {
            if (!isOpen) {

                int x = layoutMain.getRight();
                int y = layoutMain.getBottom();
                int startRadius = 0;
                int endRadius = (int) Math.hypot(layoutMain.getWidth(), layoutMain.getHeight());
                Animator anim = ViewAnimationUtils.createCircularReveal(layoutButtons, x, y, startRadius, endRadius);
                layoutButtons.setVisibility(View.VISIBLE);
                anim.start();
                isOpen = true;


            } else {

                int x = layoutButtons.getRight();
                int y = layoutButtons.getBottom();

                int startRadius = Math.max(layoutMain.getWidth(), layoutMain.getHeight());
                int endRadius = 0;
                Animator anim = ViewAnimationUtils.createCircularReveal(layoutButtons, x, y, startRadius, endRadius);
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        layoutButtons.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                anim.start();

                isOpen = false;
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
