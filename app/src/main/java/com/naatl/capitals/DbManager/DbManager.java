package com.naatl.capitals.DbManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.naatl.capitals.Models.Promo;
import com.naatl.capitals.R;

import java.util.ArrayList;

/**
 * Created by SACM on 28/10/2017.
 */

public class DbManager {
    private DbHelper helper;
    private SQLiteDatabase db;
    public static String PROMOCION ="Promocion";
    public static String ID_PROMOCION ="IdPromocion";
    public static String NOMBRE_PROMOCION ="nombrePromocion";
    public static String TIPO_PTOMOCION ="tipoPromocion";
    public static String IMAGEN="imagen";
    public static String LOGO="logo";

    public static final String CREATE_TABLE_PROMOCION = "create table " + PROMOCION + " ("
            + ID_PROMOCION + " int   ,"
            + NOMBRE_PROMOCION + " text  ,"
            + TIPO_PTOMOCION + " text  ,"
            + IMAGEN + " int  ,"
            + LOGO + " int  );";

    public DbManager(Context context) {
        helper = new DbHelper(context);
        db = helper.getWritableDatabase();
    }

    public ContentValues generaContentValues_UsuarioActivo(Promo promo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID_PROMOCION, promo.getId());
        contentValues.put(NOMBRE_PROMOCION, promo.getNombrePromocion());
        contentValues.put(TIPO_PTOMOCION, promo.getTipoPromocion());
        contentValues.put(IMAGEN, promo.getImagen());
        contentValues.put(LOGO, promo.getLogo());
        return contentValues;
    }

    public void insert_Promo(Promo promo) {
        try {
            ContentValues contentValues = generaContentValues_UsuarioActivo(promo);
            db.insert(PROMOCION, null, contentValues);
        }catch (Exception e){
            System.out.println("ERROR"+e);
        }
    }

    public Boolean get_ExistePromo(){
        try{
            String query = "select nombrePromocion from Promocion";
            Cursor cursor = db.rawQuery(query, null);Integer tam=cursor.getCount();
            if (tam>=1){
                return true;
            }else{
                return false;
            }
        }catch(Exception e){
            return false;
        }
    }

    public Cursor get_Promociones(){
        try{
            String query = "select IdPromocion,nombrePromocion,tipoPromocion,imagen,logo from Promocion";
            Cursor cursor = db.rawQuery(query, null);Integer tam=cursor.getCount();
            return cursor;
        }catch(Exception e){
            return null;
        }
    }


    public Cursor get_Promociones(String busqueda){
        try{
            String query = "select IdPromocion,nombrePromocion,tipoPromocion,imagen,logo from Promocion WHERE nombrePromocion LIKE '%"+busqueda+"%'";
            System.out.println("query "+query);
            Cursor cursor = db.rawQuery(query, null);Integer tam=cursor.getCount();
            return cursor;
        }catch(Exception e){
            return null;
        }
    }


    public Cursor get_Promo(String busqueda){
        try{
            String query = "select IdPromocion,nombrePromocion,tipoPromocion,imagen,logo from Promocion WHERE IdPromocion = "+busqueda;
            System.out.println("query "+query);
            Cursor cursor = db.rawQuery(query, null);Integer tam=cursor.getCount();
            return cursor;
        }catch(Exception e){
            return null;
        }
    }


    public void generaPromocione(){
        if(get_ExistePromo()==false) {
            ArrayList<Promo> promocions = new ArrayList<>();
            Promo promo1 = new Promo(1, "Papa John's", "20% en pizzas grandes", R.drawable.promopapajohns2, R.drawable.logopapajohns2);
            Promo promo2 = new Promo(2, "Idea Interior", "Obten 3% en tu compra total", R.drawable.promoidea2, R.drawable.logopapajohns2);
            Promo promo3 = new Promo(3, "BurgerKing", "¡Café americano chico gratis!", R.drawable.promoburguerking2, R.drawable.logopapajohns2);
            Promo promo4 = new Promo(4, "Farmacia Benavides", "10% en medicamentos genéricos", R.drawable.promobenavides2, R.drawable.logopapajohns2);
            Promo promo5 = new Promo(5, "El tizoncito", "2X1 en ¡Ah Dorados!", R.drawable.promotizoncito2, R.drawable.logopapajohns2);
            Promo promo6 = new Promo(6, "Chili's", "Ahorra 10% en el total de tu cuenta", R.drawable.promochilis2, R.drawable.logopapajohns2);
            Promo promo7 = new Promo(7, "Zona Fitness", "Precio especial en tu anualidad", R.drawable.promozonafitness2, R.drawable.logopapajohns2);
            Promo promo8 = new Promo(8, "Cinépolis", "¡GRATIS una entrada cada mes!", R.drawable.promocinepolis2, R.drawable.logopapajohns2);
            Promo promo9 = new Promo(9, "Wingstop", "$50 de descuento en la compra de $250", R.drawable.promowingstop2, R.drawable.logopapajohns2);
            Promo promo10 = new Promo(10, "Italianni's", "Ahorra 10% en el total de tu cuenta", R.drawable.promoidea2, R.drawable.logopapajohns2);

            promocions.add(promo1);
            promocions.add(promo2);
            promocions.add(promo3);
            promocions.add(promo4);
            promocions.add(promo5);
            promocions.add(promo6);
            promocions.add(promo7);
            promocions.add(promo8);
            promocions.add(promo9);
            promocions.add(promo10);

            for (Promo promo : promocions) {
                insert_Promo(promo);
            }
        }

    }

}
