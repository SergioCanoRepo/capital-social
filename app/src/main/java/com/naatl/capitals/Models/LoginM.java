package com.naatl.capitals.Models;

/**
 * Created by SACM on 28/10/2017.
 */

public class LoginM {
    private String pass;
    private String user;

    public LoginM() {
    }

    public LoginM(String pass, String user) {
        this.pass = pass;
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
