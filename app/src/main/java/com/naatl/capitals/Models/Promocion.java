package com.naatl.capitals.Models;

/**
 * Created by SACM on 28/10/2017.
 */

public class Promocion {
    private int id1;
    private String nombrePromocion1;
    private String tipoPromocion1;
    private  int imagen1;
    private int logo1;
    private int id2;
    private String nombrePromocion2;
    private String tipoPromocion2;
    private  int imagen2;
    private int logo2;

    public Promocion() {
    }

    public Promocion(int id1, String nombrePromocion1, String tipoPromocion1, int imagen1, int logo1, int id2, String nombrePromocion2, String tipoPromocion2, int imagen2, int logo2) {
        this.id1 = id1;
        this.nombrePromocion1 = nombrePromocion1;
        this.tipoPromocion1 = tipoPromocion1;
        this.imagen1 = imagen1;
        this.logo1 = logo1;
        this.id2 = id2;
        this.nombrePromocion2 = nombrePromocion2;
        this.tipoPromocion2 = tipoPromocion2;
        this.imagen2 = imagen2;
        this.logo2 = logo2;
    }

    public int getId1() {
        return id1;
    }

    public void setId1(int id1) {
        this.id1 = id1;
    }

    public String getNombrePromocion1() {
        return nombrePromocion1;
    }

    public void setNombrePromocion1(String nombrePromocion1) {
        this.nombrePromocion1 = nombrePromocion1;
    }

    public String getTipoPromocion1() {
        return tipoPromocion1;
    }

    public void setTipoPromocion1(String tipoPromocion1) {
        this.tipoPromocion1 = tipoPromocion1;
    }

    public int getImagen1() {
        return imagen1;
    }

    public void setImagen1(int imagen1) {
        this.imagen1 = imagen1;
    }

    public int getLogo1() {
        return logo1;
    }

    public void setLogo1(int logo1) {
        this.logo1 = logo1;
    }

    public int getId2() {
        return id2;
    }

    public void setId2(int id2) {
        this.id2 = id2;
    }

    public String getNombrePromocion2() {
        return nombrePromocion2;
    }

    public void setNombrePromocion2(String nombrePromocion2) {
        this.nombrePromocion2 = nombrePromocion2;
    }

    public String getTipoPromocion2() {
        return tipoPromocion2;
    }

    public void setTipoPromocion2(String tipoPromocion2) {
        this.tipoPromocion2 = tipoPromocion2;
    }

    public int getImagen2() {
        return imagen2;
    }

    public void setImagen2(int imagen2) {
        this.imagen2 = imagen2;
    }

    public int getLogo2() {
        return logo2;
    }

    public void setLogo2(int logo2) {
        this.logo2 = logo2;
    }
}
