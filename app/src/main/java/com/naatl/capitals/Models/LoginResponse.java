package com.naatl.capitals.Models;

/**
 * Created by SACM on 28/10/2017.
 */

public class LoginResponse {
    private String agente;
    private String error;
    private  int id_user;
    private String token;

    public LoginResponse() {
    }

    public LoginResponse(String agente, String error, int id_user, String token) {
        this.agente = agente;
        this.error = error;
        this.id_user = id_user;
        this.token = token;
    }

    public String getAgente() {
        return agente;
    }

    public void setAgente(String agente) {
        this.agente = agente;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
