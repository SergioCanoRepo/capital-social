package com.naatl.capitals.Models;

/**
 * Created by SACM on 28/10/2017.
 */

public class Promo {
    private int id;
    private String nombrePromocion;
    private String tipoPromocion;
    private  int imagen;
    private int logo;

    public Promo() {
    }

    public Promo(int id, String nombrePromocion, String tipoPromocion, int imagen, int logo) {
        this.id = id;
        this.nombrePromocion = nombrePromocion;
        this.tipoPromocion = tipoPromocion;
        this.imagen = imagen;
        this.logo = logo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombrePromocion() {
        return nombrePromocion;
    }

    public void setNombrePromocion(String nombrePromocion) {
        this.nombrePromocion = nombrePromocion;
    }

    public String getTipoPromocion() {
        return tipoPromocion;
    }

    public void setTipoPromocion(String tipoPromocion) {
        this.tipoPromocion = tipoPromocion;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }


}

